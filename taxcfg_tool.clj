#!/usr/bin/env bb
;;
;; This is a Babashka script. Call
;;
;; ./taxcfg_tool.clj help
;;
;; to display help information.
;;
(ns taxcfg-tool
  (:require [clojure.string :as str]
            [babashka.cli :as cli]
            [clj-yaml.core :as yaml]
            [clojure.java.io :as io]
            [clojure.edn :as edn]
            [clojure.pprint :as pp]
            [babashka.process :refer [shell]]
            [clojure.test :refer [deftest is run-tests]]
            [babashka.fs :as fs])
  (:import [clojure.lang ExceptionInfo]))

(defn table-str
  ([rows] (table-str rows {}))
  ([rows {:keys [colsep]}]
   (let [colsep (or colsep "   ")
         widths (reduce (fn [widths row]
                          (let [widths2 (map count row)]
                            (if (nil? widths)
                              widths2
                              (do (assert (= (count widths2) (count widths)))
                                  (map max widths widths2)))))
                        nil
                        rows)]
     (str/join "\n" (for [row rows]
                      (str/join colsep (map (fn [x w]
                                              (apply str x (repeat (- w (count x)) " ")))
                                            row
                                            widths)))))))

(defn kustomize-build [repo-root]
  (try
    [(-> (shell {:dir repo-root :out :string :err :string} "kustomize build .")
         :out) nil]
    (catch ExceptionInfo e
      [nil e])))

(defn parse-kustomize [s]
  {:pre [(string? s)]}
  (let [sep #{"---"}]
    (into []
          (comp (partition-by sep)
                (remove (comp sep first))
                (map (fn [lines] (yaml/parse-string (str/join "\n" lines)))))
          (str/split-lines s))))

(defn config
  ([] (config {}))
  ([extra]
   (merge {:root-dir (io/file ".")} extra)))

(defn config? [x]
  (and (map? x) (contains? x :root-dir)))

(defn overlay-dir [config]
  {:pre [(config? config)]}
  (-> config
      :root-dir
      (io/file "kustomize" "overlays")))

(defn for-every-overlay [config]
  (->> config
       overlay-dir
       .listFiles
       (map #(assoc config
                    :overlay-name (.getName %)
                    :overlay-path %))
       (sort-by (fn [x]
                  (let [name (:overlay-name x)]
                    [(if (str/starts-with? name "jobtech-taxonomy-api") 1 0)
                     (if (str/includes? name "-af-") 0 1)
                     (if (str/includes? name "test") 0 1)
                     (if (str/includes? name "prod") 1 0)])))))

(defn expect-count [n x]
  (if (= n (count x))
    x
    (throw (ex-info "Unexpected count" {:count n
                                        :x x}))))

(defn read-kustomization [config]
  (->> "kustomization.yaml"
       (io/file (:overlay-path config))
       slurp
       yaml/parse-string
       (assoc config :kustomization-src)))

(defn kustomization-commit [kustomization]
  (->> kustomization
       :images
       (map :newTag)
       (expect-count 1)
       first))


(defn read-edn [f]
  (-> f
      slurp
      edn/read-string))

(defn try-parse-edn [x]
  (try
    [(edn/read-string x)]
    (catch Exception _e nil)))

(defn write-edn [f x]
  (spit f (with-out-str (pp/pprint x))))

(defn remove-prefix [prefix s]
  (if (str/starts-with? s prefix)
    (subs s (count prefix))
    s))

(defn rebuild-config [config]
  (let [config-file (io/file (:overlay-path config) "config.edn")]
    (if (.exists config-file)
      (let [original (read-edn config-file)
            kustomization (:kustomization-src config)
            commit (kustomization-commit kustomization)
            kns (remove-prefix "jobtech-taxonomy-api-" (:namespace kustomization))

            ;; What is a good version string here?
            version (format "%s/%s" kns commit)
            
            updated (-> original
                        (assoc-in [:options :version] version))
            changed (not= original updated)]
        (when changed
          (write-edn config-file updated))
        [{:message (if changed
                     (format "Updated %s" (str config-file))
                     (format "No update of %s" (str config-file)))}])
      [{:message "No config.edn found"}])))

(defn rebuild-overlay [config]
  (-> config
      read-kustomization
      rebuild-config))

(defn rebuild [config]
  {:pre [(config? config)]}
  (vec (for [c (for-every-overlay config)
             out (rebuild-overlay c)]
         (assoc out :config c))))

(defn analyze-overlay [config]
  (merge
   config
   (let [[kust-str kust-error] (->> config
                                    :overlay-path
                                    kustomize-build)]
     (if kust-error
       {:issues [{:message "kustomize failed."}]}
       (let [kustomization (parse-kustomize kust-str)
             kind-map (group-by :kind kustomization)
             kust-config-maps (get kind-map "ConfigMap")
             config-edn (keep (comp :config.edn :data) kust-config-maps)
             invalid-edn-syntax (some (comp not try-parse-edn) config-edn)]
         {:kustomization kustomization
          :kind-map kind-map
          :issues (into []
                        cat
                        [(if (empty? config-edn)
                           [{:message "No config.edn found"}]
                           [])
                         (if invalid-edn-syntax
                           [{:message "Invalid syntax in config.edn"}]
                           [])])})))))


(defn analyze [config]
  (let [analyzed-overlays (into []
                                (map analyze-overlay)
                                (for-every-overlay config))
        issues (for [cfg analyzed-overlays
                         issue (:issues cfg)]
                     (assoc issue :config cfg))]
    {:analyzed-overlays analyzed-overlays
     :issues issues}))

;;;------- CLI -------

(defn cli-rebuild [_]
  (->> (config)
       rebuild
       (into [["OVERLAY" "MESSAGE"]]
             (map (juxt (comp :overlay-name :config) :message)))
       table-str
       println))

(defn cli-analyze [_]
  (let [issues (-> (config) analyze :issues)]
    (if (empty? issues)
      (println "No issues found.")
      (println (table-str (into [["OVERLAY" "MESSAGE"]]
                                (map (juxt (comp :overlay-name :config) :message))
                                issues))))))

(defn cli-print-help [_]
  (println "USAGE:")
  (println "./taxcfg_tool.clj rebuild")
  (println "./taxcfg_tool.clj analyze")
  (println "./taxcfg_tool.clj help"))

(def table [{:cmds ["analyze"]
             :fn cli-analyze
             :args->opts []}
            {:cmds ["rebuild"]
             :fn cli-rebuild
             :args->opts []}
            {:cmds ["test"]
             :fn (fn [& _] (run-tests))}
            {:cmds ["help"]
             :fn cli-print-help}])

(defn -main [& args]
  (cli/dispatch table args))

;;;------- Test -------
(deftest various-tests
  (let [configs (for-every-overlay (config))]
    (is (seq configs))
    (doseq [c configs]
      (is (config? c))
      (is (string? (:overlay-name c)))))
  (is (= "testing-read" (remove-prefix "jobtech-taxonomy-api-" "jobtech-taxonomy-api-testing-read")))
  (is (= "onprem" (remove-prefix "jobtech-taxonomy-api-" "onprem"))))

(deftest analyze-test
  (let [temp-dir (fs/create-temp-dir)]
    (fs/copy-tree "." temp-dir)
    (let [cfg (config {:root-dir (.toFile temp-dir)})
          overlay-map (into {}
                            (map (juxt :overlay-name identity))
                            (for-every-overlay cfg))
          analysis (analyze cfg)]
      (is (not= cfg (config)))
      (is (= [] (:issues analysis)))
      (is (seq (:analyzed-overlays analysis)))
      (let [overlay-a (overlay-map "jobtech-taxonomy-api-testing-read")
            overlay-b (overlay-map "develop")]
        (is overlay-a)
        (is overlay-b)
        (io/delete-file (io/file (:overlay-path overlay-a) "config.edn"))
        (is (= 1 (count (:issues (analyze cfg)))))
        (let [cfgfile (io/file (:overlay-path overlay-b) "config.edn")]
          (spit cfgfile (-> cfgfile
                            slurp
                            (subs 0 30))))
        (is (= 2 (count (:issues (analyze cfg)))))))))

;; Must be *after* all `deftest`.
(apply -main *command-line-args*)

