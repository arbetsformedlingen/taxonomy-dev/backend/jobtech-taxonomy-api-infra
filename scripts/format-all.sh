#!/bin/bash

# Format all config.edn files

for f in `find ../kustomize/overlays -name 'config.edn'` ; do
  ./format-edn.clj -filename $f
done
