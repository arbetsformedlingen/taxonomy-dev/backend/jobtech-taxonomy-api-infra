#!/usr/bin/env bb
(require '[clojure.java.io :as io]
         '[babashka.cli :as cli]
         '[clojure.walk :refer [postwalk]])

(defn load-edn [filename]
  (try
    (with-open [r (io/reader filename)]
      (edn/read (java.io.PushbackReader. r)))
    (catch Exception e
      (printf "Couldn't open '%s': %s\n" filename (.getMessage e)))))

(defn normalize-data [data]
  (postwalk
   (fn [x]
     (cond
       (set? x) (into (sorted-set) x)
       (map? x) (into (sorted-map) x)
       :else x))
   data))

(defn -main []
  (let [cli-options {:filename {:coerce :string}}
        {:keys [filename]} (cli/parse-opts *command-line-args* {:spec cli-options})
        edn (load-edn filename)]
    (println "normalizing: " filename)
    (spit filename (with-out-str (clojure.pprint/pprint (normalize-data edn))))))

(-main)