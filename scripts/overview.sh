#!/bin/bash

# Get an overview of the deployed commits

project="https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/jobtech-taxonomy-api"
commit_link="$project/-/commit"

for f in `find ../kustomize/overlays -name 'kustomization.yaml'` ; do
  env=$(echo $f | sed 's/\/kustomization.yaml//g ; s/.*\///g')
  commit=$(grep newTag ../kustomize/overlays/$env/kustomization.yaml | awk '{print $2}' | sed 's/"//g')
  echo "=============== " $env "    (" $commit ")"
  curl -s ${commit_link}/$commit | tee >(xq -x //title | sed 's/(.*//g') >(xq -x //time) &> /dev/null
done
