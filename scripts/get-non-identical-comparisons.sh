#!/bin/sh
# This gets all log lines that show a difference in the returned results of the multi-backend
#

echo "user: $USER"

curl -o non-identical.json -u $USER --insecure -XGET "https://10.126.2.101:9200/filebeat-*/_search?filter_path=hits.hits._source.message" -H 'Content-Type: application/json' -d'
{
  "size": 1000,
  "query": {
    "query_string": {
      "query": "DatomicVERSION2001qazwsx AND Comparing AND NOT identical",
      "default_field": "message"
    }
  }
}'

# Filter out the unimportant part of the log
# jq . non-identical.json | grep "\"message\"" | sed 's/^.*:result//g' | sort | uniq -c | sort -rn > non-identical-filtered.txt
